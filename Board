  /** @author German Viezcas
  *  @author Vanarsdall, Jeffrey L
  *  @author Tobanche, Oscar
  *  @version 0.1   
  */

  /**
  * Model class from MVC. This class contains the Tetris logic but no GUI information. 
  * Model must not know anything about the GUI and Controller.
  * @author epadilla2
  *
  */
  import java.util.HashMap;
  import java.util.Map;
  import java.util.Random;

  /** Abstraction of a 2D board as an array. Contains all information to be displayed to the player.
  */

  public class Board extends java.util.Observable{
  /**Tracks the height of the Board*/
  private static int height;
  /**Tracks Board width*/
  private static int width;
  /**Toggles between debug mode and normal operation*/
  private static boolean db = true;
  /**Tracks current level of game difficulty*/
  private static int difficulty = 0;
  /**Tracks current level being played*/
  private static int level = 0;
  /**Tracks current player score*/
  private static int score = 0;
  /**Tracks amount of lines cleared by the player*/
  private static int clearedLines = 0;
  /**Shows whether or not a piece is done moving*/
  private boolean fallFinished = false;
  /**Indicates if Tetris is active*/
  private boolean isGameActive;
  /**An individual Tetris piece @see Tetromino*/
  public Tetromino currentPiece = new Tetromino();
  /**An array containing the X coordinates of the 4 individual blocks making up a @link Tetromino*/
  private int[] currentX = new int[4];
  /**An array containing the Y coordinates of the 4 individual blocks making up a @link Tetromino*/
  private int[] currentY = new int[4];
  /**Container for any placed @link Tetromino*/
  private String[][] board;

  /**Constructor for a Board, sets all score-based metrics 
    to 0, including @link Board.difficulty, @link Board.score, @link Board.level, @link Board.clearedLines 
  @param board[][] The array where Tetrominoes are placed after they have finished falling
  */
  public Board()
  {
    this.height = 20;
    this.width = 10;
    if(db){System.out.println("Making board of height: " + this.height + " and width: " + this.width);}
    /**Note: Board.board must be called to access array elements and functions*/
    board = new String[height][width];
    if(db){System.out.println("The current board looks like " + board[0][0]);}
    for(int index = 0; index < 4; index++)
    {
      this.currentX[index] = 0;
      this.currentY[index] = 0;
    }
    for(int index = 0; index < this.height; index++)
    {
      for(int windex = 0; windex < this.width; windex++){
        board[index][windex] = "0";
      }
    }
    if(db){System.out.println("Post-fill, the board is filled with " + board[0][0]);}
    this.board = board;
    this.difficulty = 0;
    this.score = 0;
    this.level = 0;
    this.clearedLines = 0;
  }
  
  public int[] getCurrentY(){
    return this.currentY;
  }

  public int[] getCurrentX(){
    return this.currentX;
  }

  public String[][] getBoard(){
    return this.board;
  }

  /**
    * Returns true if game is active
    * @return returns @link Board.isGameActive
    */
  public boolean isGameActive()
  {
    return isGameActive;
  }

  /**
    * Returns difficulty
    * @return returns @link Board.difficulty
    */
  public int getDiff()
  {
    return this.difficulty;
  }

  /**
    * Changes/sets difficulty level
    */
  public void setDiff(int newDiff) 
  {
    this.difficulty = newDiff;
  }

  /**
    * Returns amount of lines cleared
    * @return returns @link Board.clearedLines
    */
  public int getClrLines()
  {
    return this.clearedLines;
  }

  /**
    * Sets/resets amount of lines cleared
    */
  public void setClrLines(int newClearedLines)
  {
    this.clearedLines = newClearedLines;
  }

  /**
    * Returns current score
    * @return returns @link Board.score
    */
  public int getScore()
  {
    return this.score;
  }

  /**
    * Sets/resets current score
    */
  public void setScore(int newScore)
  {
    this.score = newScore;
  }

  /**
    * Return current level
    * @return returns @link Board.level
    */
  public int getLevel()
  {
    return this.level;
  }

  /**
    * Changes the current level
    */
  public void setLevel(int newLevel)
  {
    this.level = newLevel;
  }

  /**
    * Moves tetromino down if there are no collisions. Utilizes @link #validateDown(Tetromino) for validation and @link #pieceGoesDown for the movement of the Tetromino.
    */
  public void moveTetrominoDown()
    {
    if(this.validateDown())
    {
    this.wipePiece();
    this.fillPiece();
    this.pieceGoesDown();
    if(db){System.out.println("Tetromino has moved down.");} 
    }
  }
  /**
    * Moves Current Tetromino down one level
    */
  public void moveTetrominoRight()
  {
    //your code goes here
  }
  public void moveTetrominoLeft()
  {
    //your code goes here
  }

  
  private boolean validateDown()
  {
    // check if board is filled at the position tetromino is attempting to move to
    // return true if it is a valid position, i.e., board at said index is equal to zero
    int [] ychecker = this.getCurrentY();
    int [] xchecker = this.getCurrentX();

    for(int index = 0; index < 4; index++)
    if(db){System.out.println("Y Coordinate " + index + " is " + ychecker[index]);}

    boolean[] cond1 = new boolean[4];
    boolean[] cond2 = new boolean[4];

    for(int index = 0; index < 4; ++index)
    {
      cond1[index] = true;
      cond2[index]= true;
    }

    for(int index = 0; index < 4; ++index) 
    { 
       if(db){System.out.println("Y value being adjusted: " + ychecker[index]);}
      ychecker[index] = (ychecker[index] + 1);
       if(db){System.out.println("Post-adjustment Y value is " + ychecker[index]);}
    
      if (ychecker[index] < 0 || ychecker[index] > 20) 
      {
      cond1[index] = false;
      return false;
      }
          if(db){System.out.println("Coordinates to be validated are: (" + xchecker[index] + " ," + ychecker[index] + ").");}
      if(!this.board[ychecker[index]][xchecker[index]].equals("0"))
      {
      this.fallFinished = true;
      cond2[index] = false;
      }
    }

    for(int index = 0; index < 4; ++index)
    {
    if(cond1[index] == false || cond2[index] == false)
    {return false;}
    }
  if(db){System.out.println("downward dimension validated");}
  return true;
    }
  /**
    * Returns if this is a valid position
    * @return
    */
  

  private void pieceGoesDown()
  {
    
  for(int index = 0; index < 4; ++index)
  {
    currentY[index] = currentY[index] + 1; 
  }
  
  if(fallFinished) 
  {
    this.newPiece();
  }
  if(db){System.out.println("pieceGoesDown() complete");}
  }
    /**
    * Moves piece down
    */
  
  public void newPiece()
  {
    if(db){System.out.println("Attempting to create a new piece.");
    if(currentPiece == null){System.out.println("Current Piece has an enum that is " + this.currentPiece.getEnum());}}

  
      this.currentPiece = currentPiece.makeTetromino(this.currentPiece.getEnum());
      if(db){System.out.println("Current Piece has an enum that is " + this.currentPiece.getEnum());}
    

    this.coordinateFiller();

    for(int i = 3; i < 7; i++)
    {
    if(!this.board[0][i].equals("0")){
      System.out.println("GAME OVER");
    }
    }
    this.fillPiece();
    if(db){System.out.println("newPiece() complete");}
  }
  /**
    * Attempts to create a new piece, prints Game Over
    * message to console
    */

    

    public void printBoard(){
        String[][] k = new String[20][10];
        k = this.board;
        if(db){System.out.println("The board k is currently filled with " + k[0][0]);}
        int rows = this.height;
        int columns = this.width;
        int diff = 0;
        int level = 0;
        int linesCleared = 4;
        int score = 8;
        String str = "\t" + "\t" + "| ";
        String str2 = "\t" + "\t" + "+";
        System.out.println(" ");

        System.out.println("\t" + "\t" + "Difficulty: " + diff + "\t" + "\t" + "\t" + "\t" + "\t" + "Level: " + level);
        System.out.println(" ");
        System.out.println("\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "Lines: " + linesCleared);
        System.out.println("");
        System.out.println("\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "Score: " + score);
        System.out.println("");
          for(int i=0;i<rows;i++){
              for(int j=0;j<columns;++j){


                
                  str += k[i][j] + " | ";
                  str2 += "---+";
              }

              System.out.println(str);
              System.out.println(str2);
              str = "\t" + "\t" + "| ";
              str2 = "\t" + "\t" + "+";
          }
          if(db){System.out.println("printBoard() complete");}
        }


  public void fillPiece()
  {
    int x = 0;
    int y = 0;
    // Get coord arrays
    // fill board at coords with tetro type
    for(int i = 0; i < 4; ++i){
      y = this.currentY[i];
      x = this.currentX[i];
      if(db){System.out.println("Placing " + this.currentPiece.getEnum().toString() + "at (" + x + ", " + y + ")");}

    this.board[y][x] = (this.currentPiece.getEnum().toString());
    currentY[i] = y;
    currentX[i] = x;
      y = 0;
      x = 0;
      if(db){System.out.println("piece Coordinates filled on board");}
    }
  }

  public void wipePiece()
  {
    int x = 0;
    int y = 0;
    for(int i = 0; i < 4; i++){
      x = this.currentX[i];
      y = this.currentY[i]-1;
      if(db){System.out.println("Wiping board at (" + x + ", " + y + ")");}

      this.board[y][x] = "0";
      //if(db){System.out.println("Placing 1 at (" + x + ", " + y + ")");}
      y = 0;
      x = 0;
      if(db){System.out.println("Previous piece placement cleared");}
    }
  }
    /**
    * Prints board to console in easy-to-read format
    * if Boolean "db" is true, an error message is printed
    * to console
    */

      
  private void coordinateFiller()
  {
    String piece = this.currentPiece.getEnum().toString();
    switch(piece)
    {
    case "I":
    this.currentX[0] = 3;
    this.currentX[1] = 4;
    this.currentX[2] = 5;
    this.currentX[3] = 6;
    
    for(int i = 0; i < 4; ++i) {
    currentY[i]= 0;
    }
    break;

    case "J":
    this.currentX[0] = 3;
    this.currentX[1] = 3;
    this.currentX[2] = 4;
    this.currentX[3] = 5;
    
    this.currentY[0] = 0;
    this.currentY[1] = 1;
    this.currentY[2] = 1;
    this.currentY[3] = 1;
    break;

    case "L":
    this.currentX[0] = 3;
    this.currentX[1] = 4;
    this.currentX[2] = 5;
    this.currentX[3] = 5;
    
    this.currentY[0] = 1;
    this.currentY[1] = 1;
    this.currentY[2] = 1;
    this.currentY[3] = 0;
    break;

    case "O":
    this.currentX[0] = 3;
    this.currentX[1] = 3;
    this.currentX[2] = 4;
    this.currentX[3] = 4;
    
    this.currentY[0] = 0;
    this.currentY[1] = 1;
    this.currentY[2] = 0;
    this.currentY[3] = 1;
    break;

    case "S":
    this.currentX[0] = 3;
    this.currentX[1] = 4;
    this.currentX[2] = 4;
    this.currentX[3] = 5;
    
    this.currentY[0] = 1;
    this.currentY[1] = 1;
    this.currentY[2] = 0;
    this.currentY[3] = 0;
    break;

    case "Z":
    this.currentX[0] = 3;
    this.currentX[1] = 4;
    this.currentX[2] = 4;
    this.currentX[3] = 5;
    
    this.currentY[0] = 0;
    this.currentY[1] = 0;
    this.currentY[2] = 1;
    this.currentY[3] = 1;
    break;

    case "T":
    this.currentX[0] = 3;
    this.currentX[1] = 4;
    this.currentX[2] = 4;
    this.currentX[3] = 5;
    
    this.currentY[0] = 1;
    this.currentY[1] = 0;
    this.currentY[2] = 1;
    this.currentY[3] = 1;
    break;

    default:
    this.currentX[0] = 3;
    this.currentX[1] = 4;
    this.currentX[2] = 4;
    this.currentX[3] = 5;
    
    this.currentY[0] = 1;
    this.currentY[1] = 0;
    this.currentY[2] = 1;
    this.currentY[3] = 1;

    if(db){System.out.println("coordinates of newPiece() filled");}
    }
    /**
    * Hard Codes starting coordinates based on piece type
    */
    
  }
  }