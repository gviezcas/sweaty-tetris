import cs3331.controller.Controller;
import cs3331.model.GameState;
import cs3331.view.TetrisUI;


/**
 * @author Jeffrey Vanarsdall
 * @author German Viezcas
 * @author Oscar Tobanche
 * 
 *
 */
public class Main
{
  public Main() {}
  
  /**
   * The main method for running an instance of Tetris
   * @param args The String array containing arguments
   */
  public static void main(String[] args)
  {
   /**
    * Creates a new {@link cs3331.model.GameState}
    */
    GameState gameState = new GameState();
    
    /**
     * Creates a new {@link cs3331.controller.Controller}
  */
    Controller controller = new Controller();
    /**
   * Establishes communication between the {@link cs3331.controller.Controller} and {@link cs3331.model.GameState}
   */
    controller.setModel(gameState);
    /**
     * Creates a {@link cs3331.view.TetrisUI} based on the parameters passed in the arguments 
     */
    TetrisUI gui = new TetrisUI(args);
    /**
     * Creates an Action Listener for actions performed by the {@link cs3331.controller.Controller}
     */
    gui.setActionListener(controller);
    /**
     * Creates a Key Listener for inputs performed on the {@link cs3331.controller.Controller}
     */
    gui.setKeyListener(controller);
    
    gui.getNetworkInterface().addWindowListener(controller);
    gui.getNetworkInterface().run();
    gui.getNetworkInterface().setActionListener(controller);
    
    /**
     * Establishes communication between the {@link cs3331.view.TetrisUI} and the {@link cs3331.model.GameState}
     */
    gui.setModel(gameState);
    /**
     * Establishes communication between the {@link cs3331.controller Controller} and the {@link cs3331.view.TetrisUI}
     */
    controller.setView(gui);
    /**
     * Allows the {@link cs3331.view.TetrisUI} to generate an interface based on information in the {@link cs3331.model.GameState}
     */
    gameState.addObserver(gui);
    /**
     * Gives the command that starts the game
     */
    gui.run();
  }
}