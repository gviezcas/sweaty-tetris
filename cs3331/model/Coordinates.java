package cs3331.model;

/**
 * @author Jeffrey Vanarsdall
 * @author German Viezcas
 * @author Oscar Tobanche
 *
 * @param <X> {@link #X} component of a {@link Coordinate}
 * @param <Y> {@link #Y} component of a {@link Coordinate}
 */

/**
 * The constructor for a {@link Coordinate}
 */
public class Coordinates<X, Y> {
  private final X xValue;
  private final Y yValue;
  
  public Coordinates(X xValue, Y yValue) {
    this.xValue = xValue;
    this.yValue = yValue;
  }
  
  /** A helper method that returns the {@link #X} of a {@link Coordinate}
   * 
   * @return {@link #X}
   * @see #getY()
   */
  public X getX()
  {
    return xValue;
  }
  
  /** A helper method that returns the {@link #Y} of a {@link Coordinate}
   * 
   * @return {@link #Y}
   * @see #getX()
   */
  public Y getY() {
    return yValue;
  }
}
