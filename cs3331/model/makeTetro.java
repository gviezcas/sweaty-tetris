package cs3331.model;

public class makeTetro{
/**
 * Empty constructor for a makeTetro  
 */
  public makeTetro(){}
  
  /** This class goes through a switch statement to create the
    * desired Tetromino shape.
    * @param tetroEnum the enum that associates integer values with {@link cs3331.model.Tetromino}
    * @return tetro a {@link cs3331.model.Tetromino}
    */
  public Tetromino makeTetromino(Tetromino.TetrominoEnum tetroEnum) 
  {
    Tetromino tetro = null;
    switch (tetroEnum)
    {
      case I:
        tetro = new Itetro();
        break;
      case J:
        tetro = new Jtetro();
        break;
      case L:
        tetro = new Ltetro();
        break;
      case O:
        tetro = new Otetro();
        break;
      case S:
        tetro = new Stetro();
        break;
      case Z:
        tetro = new Ztetro();
        break;
      case T:
        tetro = new Ttetro();
        break;
      case FILLER:
        tetro = new Ttetro();
        break;
      default:
        return tetro;
    }
    return tetro;
  }
  
  /** The class for an I Tetromino that fills a position array that defines it's shape* */
  private class Itetro extends Tetromino
  {
    Itetro()
    {
      int[][] shape = new int[][]{{1,1,1,1}};
      this.position = shape;
      this.tetroEnum = Tetromino.TetrominoEnum.I;
    }
  }
  
  /** The class for a J Tetromino that fills a position array that defines it's shape* */
  private class Jtetro extends Tetromino
  {
    Jtetro()
    {
      int[][] shape = new int[2][];
      int[] array = new int[3];
      array[0] = 1;
      shape[0] = array;
      shape[1] = new int[]{1, 1, 1};
      int[][] coords = shape;
      this.position = coords;
      this.tetroEnum = Tetromino.TetrominoEnum.J;
    }
  }
  
  /** The class for an L Tetromino that fills a position array that defines it's shape* */
  private class Ltetro extends Tetromino
  {
    Ltetro()
    {
      int[][] shape = new int[2][];
      int[] array = new int[3];
      array[2] = 1;
      shape[0] = array;
      shape[1] = new int[]{1, 1, 1};
      int[][] coords = shape;
      this.position = coords;
      this.tetroEnum = Tetromino.TetrominoEnum.L;
    }
  }
  
  /** The class for an O Tetromino that fills a position array that defines it's shape* */
  private class Otetro extends Tetromino
  {
    Otetro()
    {
      int[][] shape = new int[][]{{1,1},{1,1}};
      this.position = shape;
      this.tetroEnum = Tetromino.TetrominoEnum.O;
    }
  }
  
  /** The class for an S Tetromino that fills a position array that defines it's shape* */
  private class Stetro extends Tetromino
  {
    Stetro()
    {
      int[][] shape = new int[2][];
      int[] array = new int[3];
      array[1] = 1;
      array[2] = 1;
      shape[0] = array;
      int[] array2 = new int[3];
      array2[0] = 1;
      array2[1] = 1;
      shape[1] = array2;
      int[][] coords = shape;
      this.position = coords;
      this.tetroEnum = Tetromino.TetrominoEnum.S;
    }
  }
  
  /** The class for a Z Tetromino that fills a position array that defines it's shape* */
  private class Ztetro extends Tetromino
  {
    Ztetro()
    {
      int[][] shape = new int[2][];
      int[] array = new int[3];
      array[0] = 1;
      array[1] = 1;
      shape[0] = array;
      int[] array2 = new int[3];
      array2[1] = 1;
      array2[2] = 1;
      shape[1] = array2;
      int[][] coords = shape;
      this.position = coords;
      this.tetroEnum = Tetromino.TetrominoEnum.Z;
    }
  }
  /** The class for a T Tetromino that fills a position array that defines it's shape* */
  private class Ttetro extends Tetromino
  {
    Ttetro()
    {
      int[][] shape = new int[2][];
      int[] array = new int[3];
      array[1] = 1;
      shape[0] = array;
      shape[1] = new int[]{1, 1, 1};
      int[][] coords = shape;
      this.position = coords;
      this.tetroEnum = Tetromino.TetrominoEnum.T;
    }
  }
  
    /** Generates a random Tetromino.
      * @return a {@link cs3331.model.Tetromino} of random type
      */
  public Tetromino getRandomTetromino()
  {
    return makeTetromino(Tetromino.TetrominoEnum.getRandomTetromino());
  }
}