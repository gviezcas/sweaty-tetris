package cs3331.model;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
/**
 * 
 * 
 * @author Jeffrey Lynn Vanarsdall
 * @author German Viezcas
 * @author Oscar Tobanche
 */
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Iterator;
import java.util.Observable;
import cs3331.model.Tetromino;

/**
 * 
 * Constructor for a {@link GameState}
 *
 */
public class GameState extends java.util.Observable{
  /**Tracks the {@link #height} of the Board*/
  private static int height;
  /**Tracks Board {@link #width}*/
  private static int width;
  /**Toggles between debug mode and normal operation*/
  private static boolean db = true;
  /**Tracks current {@link #level} of game difficulty*/
  private static int difficulty = 0;
  /**Tracks current {@link #level} being played*/
  private static int level = 0;
  /**Tracks current player {@link #score}*/
  private static int score = 0;
  /**Tracks amount of {@link #clearedLines} by the player*/
  private static int clearedLines = 0;
  /**Shows whether or not a piece is done moving*/
  private boolean fallFinished = false;
  /**Indicates if game is active*/
  private boolean isGameActive;
  /**Indicates if game is over*/
  private boolean isGameOver;
  /**An individual game piece {@link cs3331.model.Tetromino}*/
  public Tetromino currentPiece = new Tetromino();
  /**The tracked {@link cs3331.model.Coordinate#X} of a {@link cs3331.model.Tetromino}*/
  private int currentX;
  /**The tracked {@link cs3331.model.Coordinate#Y} of a {@link cs3331.model.Tetromino}*/
  private int currentY;
  /**Container for any placed {@link cs3331.model.Tetromino}*/
  private Tetromino.TetrominoEnum[][] board;
  /**Object to create new {@link cs3331.model.Tetromino} piece {@link cs3331.model.makeTetro}*/
  public makeTetro mt;
  /**Next {@link cs3331.model.Tetromino} to show on the board.*/
  private Tetromino nextTetro = null;
  /**Indicates how much space of the {@link #board} is being used by the active {@link cs3331.model.Tetromino}*/
  private int boardUsedSpace = 0;
  /**Indicates whether or not{@link #score} needs to be updated */
  private boolean newScoreAvailable;
  /**Indicates whether the player has earned an increase in {@link #level} */
  private boolean levelUpgrade;
  /**Indicates whether or not the active {@link cs3331.model.Tetromino} is moving */
  private boolean advancingTetromino;
  
  private String playerName;
  private Tetromino.TetrominoEnum[][] otherBoard;
  private boolean isMultiplayerGame;
  private String otherPlayerName;
  private int otherScore;
  private boolean isOtherGameOver;
  private NetworkAdapter networkAdapter = null;
  
  /**Empty Constructor for a {@link GameState} */
  public GameState() {}
  
  /** Starts a new Single Player instance of the game */
  public void newSinglePlayerGame() {
    newGame();
  }
  
  /**Constructor for a new game, sets all score-based metrics to default, including {@link #difficulty}, {@link #score}, {@link #level}, {@link #clearedLines} 
    *
    * 
    */
  private void newGame() {
 /**Creates the {@link #board} */
    board = new Tetromino.TetrominoEnum[20][10];
    /**Variable for holding and randomizing the next {@link cs3331.model.Tetromino}*/
    mt = new makeTetro();
    nextTetro = mt.getRandomTetromino();
    /**Gives the player control over the {@link cs3331.model.Tetromino} */
    advanceNextTetromino();
    /**Sets the game to default levels and conditions */
    level = 1;
    score = 0;
    clearedLines = 0;
    isGameActive = true;
    levelUpgrade = false;
    isGameOver = false;
  }
  
  /**
   * Designates the second player's board as the {@link #otherBoard}
   * @param otherBoard the second player's board
   */
  public void setOtherBoard(Tetromino.TetrominoEnum[][] otherBoard) {
    this.otherBoard = otherBoard;
  }
  /**
   * Returns the {@link #otherBoard}
   * @return otherBoard the second player's board
   */
  public Tetromino.TetrominoEnum[][] getOtherBoard() {
    return otherBoard;
  }
  /**
   * Returns {@link #isMultiplayerGame}
   * @return isMultiplayerGame the indicator that the current game is multiplayer
   */
  public boolean isMultiPlayerGame() {
    return isMultiplayerGame;
  }
  /**
   * Returns {@link #otherPlayerName}
   * @return otherPlayerName the name entered by the second player
   */
  public String getOtherPlayerName() {
    return otherPlayerName;
  }
  /**
   * Sets the name provided by the second player as {@link #otherPlayerName}
   * @param otherPlayerName the name provided by the second player
   */
    public void setOtherPlayerName(String otherPlayerName) {
    this.otherPlayerName = otherPlayerName;
  }
  /**
   * Sets the second player's score as {@link #otherScore} 
   * @param otherScore the score of the second player
   */
  public void setOtherScore(int otherScore) {
    this.otherScore = otherScore;
  }
  /**
   * Returns {@link #otherScore}
   * @return otherScore the current score of the second player
   */
  public int getOtherScore() {
    return otherScore;
  }
  /**
   * Returns {@link #isOtherGameOver}
   * @return isOtherGameOver the indicator that the second player's game has ended
   */
  public boolean isOtherGameOver() {
    return isOtherGameOver;
  }
  /**
   * Sets the second player's game over indicator to {@link #isOtherGameOver}
   * @param isOtherGameOver the indicator that the second player's game is over
   */
  public void isOtherGameOver(boolean isOtherGameOver) {
    this.isOtherGameOver = isOtherGameOver;
  }
  /**
   * Sets the {@link #networkAdapter}
   * @param networkAdapter a Network Adapter
   */
  public void setNetworkAdapter(NetworkAdapter networkAdapter)
  {
    this.networkAdapter = networkAdapter;
  }
  /**
   * Returns the {@link #networkAdapter}
   * @return networkAdapter the network adapter of the TetrisUI
   */
  public NetworkAdapter getNetworkAdapter() {
    return networkAdapter;
  }
  /**
   * Starts a new multiplayer game
   */
  public void newMultiPlayerGame() {
    isMultiplayerGame = true;
    newGame();
  }
  /**
   * Helper method that returns the {@link #currentY} of a {@link cs3331.model.Tetromino}
   * @return {@link #currentY} The tracked Y coordinate of a {@link Tetromino}
   */
  public int getCurrentY(){
     return this.currentY;
   }
  /**
   * Helper method that returns the {@link #currentX} of a {@link cs3331.model.Tetromino}
   * @return {@link #currentX} The tracked X coordinate of a {@link Tetromino}
   */
  public int getCurrentX(){
     return this.currentX;
   }
   
  
  /**
   * Helper method that returns a copy of the current {@link #board}
   * @return boardCopy, a deep clone of the original {@link #board}
   */
  public Tetromino.TetrominoEnum[][] getBoard() {
    Tetromino.TetrominoEnum[][] boardCopy = new Tetromino.TetrominoEnum[board.length][];
    for (int y = 0; y < board.length; y++) {
      boardCopy[y] = ((Tetromino.TetrominoEnum[])board[y].clone());
    }
    Tetromino.TetrominoEnum type = currentPiece.getType();
    Iterator<Coordinates<Integer, Integer>> iter = currentPiece.iterator();
    while (iter.hasNext())
    {
      Coordinates<Integer, Integer> coords = (Coordinates)iter.next();
      boardCopy[(currentY + ((Integer)coords.getY()).intValue())][(currentX + ((Integer)coords.getX()).intValue())] = type;
    }
    
    return boardCopy;
  }
  
  /**
   * Helper method that returns the {@link #nextTetro} that will be controlled by the player
   * @return {@link #nextTetro} the next Tetromino
   */
  public Tetromino getNextTetro() {
    return nextTetro;
  }
  
  /**
   * Helper method that returns true if the game is active
   * @return {@link #isGameActive} the indicator that shows true if the game is running
   */
  public boolean isGameActive()
  {
    return isGameActive;
  }
  /**
   * Helper method that returns true if game is over
   * @return {@link #isGameOver} the indicator that shows true if the game has ended
   */
  public boolean isGameOver() {
    return isGameOver;
  }
  
  /**
   * Helper method that returns {@link #clearedLines}
   * @return {@link #clearedLines} the amount of lines the player has cleared
   */
  public int getClrLines()
  {
    return this.clearedLines;
  }
  
  /**
   * Mutator method to update the score board with the amount of lines the player has cleared
   * @param newClearedLines the amount of lines the player has cleared
   */
  public void setClrLines(int newClearedLines)
  {
    this.clearedLines = newClearedLines;
  }
  
  /**
   * Helper method that returns the {@link #score}
   * @return {@link #score} the player's current score
   */
  public int getScore()
  {
    return this.score;
  }
  
  /**
   * Mutator method to update the score board with the player's {@link #score}
   * @param newScore the player's current score
   */
  public void setScore(int newScore)
  {
    this.score = newScore;
  }
  
  /**
   * Helper method that returns the current {@link #level}
   * @return {@link #level} the level the player is playing through
   */
  public int getLevel()
  {
    return this.level;
  }
  
  /**
   * Mutator method to update the score board with the {@link #level} the player is on
   * @param newLevel the players current level
   */
  public void setLevel(int newLevel)
  {
    this.level = newLevel;
  }
  
  public void setPlayerName(String playerName){
   this.playerName = playerName;
  }
  
  public String getPlayerName(){
   return playerName;
  }
  
  /**
   * Helper method that returns true if the player has earned a level increase
   * @return temp the indicator of whether or not the player has earned a level increase
   */
  public boolean levelUpgrade() {
    boolean temp = levelUpgrade;
    levelUpgrade = false;
    return temp;
  }
  
  /**
   * Moves the {@link cs3331.model.Tetromino} one unit to the right
   */
  public void moveTetrominoRight()
  {
    currentX = (currentX + 1);
    if (!validatePosition())
      currentX = (currentX - 1);
  }
  
  /**
   * Moves the {@link cs3331.model.Tetromino} one unit to the left
   */
  public void moveTetrominoLeft()
  {
    currentX = (currentX - 1);
    if (!validatePosition())
      currentX = (currentX + 1);
  }
  /**
   * Hands the player control over the next randomly generated {@link cs3331.model.Tetromino}
   */
  private void advanceNextTetromino() {
    currentPiece = nextTetro;
    nextTetro = mt.getRandomTetromino();
    currentX = 4;
    currentY = 0;
    boardUsedSpace += 4;
  }
  /**
   * Helper method that returns true if the player's {@link #score} needs to be updated
   * @return temp the indicator that the player's score has changed
   */
  public boolean newScoreAvailable() {
    boolean temp = newScoreAvailable;
    newScoreAvailable = false;
    return temp;
  }
  /**
   * Moves a {@link cs3331.model.Tetromino} to the next position
   */
  public synchronized void advanceTetromino()
  {
    advancingTetromino = true;
    moveTetrominoDown();
    advancingTetromino = false;
  }
  /**
   * Moves a {@link cs3331.model.Tetromino} down one unit
   */
    public synchronized void moveTetrominoDown()
  {
    currentY = (currentY + 1);
    validatePosition();
    newScoreAvailable = true;
    score += 5;
  }
  /**
   * Rotates a {@link cs3331.model.Tetromino} ninety degrees clockwise
   */
  public synchronized void rotateTetrominoRight() {
    currentPiece.rotateRight();
    if (!validatePosition())
      currentPiece.rotateLeft();
  }
  /**'
   * Rotates a {@link cs3331.model.Tetromino} ninety degrees counterclockwise
   */
  public synchronized void rotateTetrominoLeft() {
    currentPiece.rotateLeft();
    if (!validatePosition()) {
      currentPiece.rotateRight();
    }
  }
  /**
   * Helper method that returns true if a {@link cs3331.model.Tetromino}'s position would cause a collision
   * @param coords the coordinates of a {@link cs3331.model.Tetromino}'s current position
   * @return board[int][int] the indicator that a collision will occur
   */
  private boolean doesOverlapCurrent(Coordinates<Integer, Integer> coords) {
    return board[(currentY + ((Integer)coords.getY()).intValue())][(currentX + ((Integer)coords.getX()).intValue())] != null;
  }
  /**
   * Helper method that returns true if a {@link cs3331.model.Tetromino}'s next position would cause a collision
   * @param coords the coordinates of a {@link cs3331.model.Tetromino}'s next position
   * @return board[int][int] the indicator that a collision will occur or the indicator that the next position is beyond the boundaries of the {@link #board}
   */
  private boolean doesOverlapNext(Coordinates<Integer, Integer> coords) {
    return (currentY + ((Integer)coords.getY()).intValue() + 1 > 19) || (board[(currentY + ((Integer)coords.getY()).intValue() + 1)][(currentX + ((Integer)coords.getX()).intValue())] != null);
  }
  
  /**
   * Evaluates the position of a {@link cs3331.model.Tetromino}'s movements and returns true if a collision would be created
   * @return the indicator that shows whether or not a collision will occur
   */
  private boolean validatePosition()
  {
    if ((currentX < 0) || (currentX + currentPiece.getWidth() - 1 > 9)) {
      return false;
    }
    
    if ((currentY < 0) || (currentY + currentPiece.getHeight() - 1 > 19)) {
      return false;
    }
    Iterator<Coordinates<Integer, Integer>> iter = currentPiece.iterator();
    while (iter.hasNext())
    {
      Coordinates<Integer, Integer> coords = (Coordinates)iter.next();
      
      if (doesOverlapCurrent(coords)) {
        return false;
      }
      
      if (doesOverlapNext(coords))
      {
        if (currentY <= 1)
        {
          gameOver();
          return false;
        }
        //timer here
        long startTime = System.currentTimeMillis();
        for(int i = 0; i < 720000; i++) {
        	
        }
        incorporateTetromino();
        checkForCompletedLines();
        
        advanceNextTetromino();
        if (!validatePosition())
        {
          if (isMultiplayerGame)
            sendBoardOverNetwork();
          return false;
        }
        setChanged();
        notifyObservers();
        return true;
      }
    }
    if (isMultiplayerGame)
      sendBoardOverNetwork();
    return true;
  }

  /**
   * Method that incorporates a {@link cs3331.model.Tetromino} into the current arrangement 
   */
  private void incorporateTetromino()
  {
    Tetromino.TetrominoEnum type = currentPiece.getType();
    Iterator<Coordinates<Integer, Integer>> iter = currentPiece.iterator();
    while (iter.hasNext())
    {
      Coordinates<Integer, Integer> coords = (Coordinates)iter.next();
      
      board[(currentY + ((Integer)coords.getY()).intValue())][(currentX + ((Integer)coords.getX()).intValue())] = type;
    }
    
    newScoreAvailable = true;
    score += 5;
  }
  /**
   *  Method that checks the current arrangement for any complete lines and signals for a scoreboard update
   */
  private void checkForCompletedLines()
  {
    int completedLinesAtOnce = 0;
    for (int y = 0; y < 20; y++)
    {
      boolean completedLine = true;
      for (int x = 0; x < 10; x++)
        if (board[y][x] == null)
      {
        completedLine = false;
        break;
      }
      if (completedLine)
      {
        removeLine(y);
        
        clearedLines += 1;
        if (clearedLines % 5 == 0)
        {
          level += 1;
          levelUpgrade = true;
        }
        completedLinesAtOnce++;
        System.out.println(completedLinesAtOnce);
        newScoreAvailable = true;
        
        y--;
      }
    }
    switch (completedLinesAtOnce)
    {
      case 1: 
        score += 100;
        break;
      case 2: 
        score += 300;
        if (isMultiplayerGame)
          networkAdapter.writeFill(1);
        break;
      case 3: 
        score += 600;
        if (isMultiplayerGame)
          networkAdapter.writeFill(2);
        break;
      case 4: 
        score += 1000;
        if (isMultiplayerGame)
          networkAdapter.writeFill(4);
        break;
    }
  }
  /** 
   * Method that adds lines back to the {@link #board} after they've been cleared
   * @param additionalLines the amount of lines the player just cleared
   */
public void fillBoard(int additionalLines) {
    Random random = new Random();
    for (int i = 0; i < additionalLines; i++)
    {

      for (int y = 1; y < 20; y++) {
        board[(y - 1)] = board[y];
      }
      board[19] = new Tetromino.TetrominoEnum[10];
      for (int x = 0; x < 10; x++) {
        board[19][x] = Tetromino.TetrominoEnum.FILLER;
      }
      board[19][random.nextInt(10)] = null;
      boardUsedSpace += 9;
    }
  }
/**
 * Method that removes an amount of lines from the {@link #board} that the player filled with {@link cs3331.model.Tetromino}es
 * @param yLine the amount of lines to be removed
 */
private void removeLine(int yLine)
  {
    for (int y = yLine; y > 0; y--)
    {
      board[y] = board[(y - 1)];
    }
    
    board[0] = new Tetromino.TetrominoEnum[10];
    boardUsedSpace -= 10;
  }


/**
 * Mutator method that adjusts {@link #isGameActive} and {@link #isGameOver} to indicate the game has ended
 */
private void gameOver() {
    isGameActive = false;
    isGameOver = true;
    setChanged();
    notifyObservers();
  }
/**
 * Sends the updated board between first and second players
 */
public void sendBoardOverNetwork() {
    Tetromino.TetrominoEnum[][] board = getBoard();
    int[] boardCoordinates = new int[boardUsedSpace * 3];
    int counter = 0;
    for (int y = 0; y < 20; y++) {
      for (int x = 0; x < 10; x++)
        if (board[y][x] != null)
        {
          boardCoordinates[(counter * 3)] = x;
          boardCoordinates[(counter * 3 + 1)] = y;
          boardCoordinates[(counter * 3 + 2)] = board[y][x].getValue();
          counter++;
        }
    }
    networkAdapter.writeStatus(score, isGameOver ? 1 : 0, boardCoordinates);
  }
  
}