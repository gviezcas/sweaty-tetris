package cs3331.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


/** This class is the basis for a Tetromino.
  * @author German Viezcas
  * @param <U>
  * @param <T>
  *
  */
public class Tetromino implements Iterable<Coordinates<Integer, Integer>>
{
  
  protected int[][] position;  //Array to hold values and shapes of Tetromino
  protected TetrominoEnum tetroEnum;  //Enumeration object
  
  /**
   * Returns the Letter Representation of a Tetromino's shape.
   * @return {@link #tetroEnum} the Letter Representation of a Tetromino's shape
   */
  public TetrominoEnum getType(){
    return tetroEnum;
  }
  /**
   * Returns the width of the {@link #position} array
   * @return position[0].length the length of the {@link #position} array
   * @see #getHeight()
   */
  public int getWidth() {
    return position[0].length;
  }
  /**
   * Returns the {@link #height} of the {@link #position} array
   * @return position.length the height of the {@link #position} array
   * @see #getWidth()
   */
  public int getHeight() {
    return position.length;
  }
  
  /**
   * Rotates a {@link Tetromino} 90 degrees clockwise.
   */
  public void rotateRight()
  {
    int[][] rotatedPosition = new int[position[0].length][position.length];
    for (int y = 0; y < position.length; y++)
      for (int x = 0; x < position[0].length; x++)
      rotatedPosition[x][(position.length - 1 - y)] = position[y][x];
    position = rotatedPosition;
  }
  /**
   * Rotates a {@link Tetromino} 90 degrees counterclockwise.
   */
  public void rotateLeft()
  {
    int[][] rotatedPosition = new int[position[0].length][position.length];
    for (int y = 0; y < position.length; y++)
      for (int x = 0; x < position[0].length; x++)
      rotatedPosition[(position[0].length - 1 - x)][y] = position[y][x];
    position = rotatedPosition;
  }
  /**
   * Iterator to sequentially access all coordinates of the {@link #position} array
   * @return
   */
  public Iterator<Coordinates<Integer, Integer>> iterator() {
    
    List<Coordinates<Integer, Integer>> list = new LinkedList();
    

    for (int x = 0; x < position[0].length; x++) {
      for (int y = 0; y < position.length; y++)
      {
        int value = position[y][x];
        if (value == 1)
          list.add(new Coordinates(new Integer(x), new Integer(y)));
      }
    }
    return list.iterator();
  }
  
  /**
   * Enumeration to list the different tetrominos.
   * FILLER used when in multiplayer sending a line to the other player.
   * @author epadilla2
   *
   */
  public enum TetrominoEnum 
  {
    /** Types of tetrominos, filler represents punishment lines added in multiplayer mode */
    
    I(0), J(1), L(2), O(3), S(4), Z(5), T(6), FILLER(7);
    /** Integer value of each tetromino*/
    private int value;
    /**  Hash for inverse lookup of a tetromino based on value*/
    private static final Map<Integer, TetrominoEnum> reverseLookup = new HashMap<Integer, TetrominoEnum>();
    
    static {
      for (TetrominoEnum tetromino : TetrominoEnum.values()) {
        reverseLookup.put(tetromino.getValue(), tetromino);
      }
    }
    /**
     * Constructor that sets the integer value of a {@link Tetromino} 
     * @param {@link #value} integer value of a {@link Tetromino} 
     */
    TetrominoEnum(int value)
    {
      this.value = value;
    }
    /**
     * Returns the {@link #value} of a {@link Tetromino}
     * @return value The integer value of a {@link Tetromino}
     */
    public int getValue()
    {
      return value;
    }
    /**
     * Return TetrominoEnum depending on value
     * @param {@link value} the integer {@link #value} of the {@link Tetromino}
     * @return reverseLookup.get(value) the {@link #tetroEnum} associated with a given {@link #value}
     */
    public static TetrominoEnum getEnumByValue(int value)
    {
      return reverseLookup.get(value);
    }
    /**
     * Returns a random TetrominoEnum
     * @return random A randomly selected {@link tetroEnum}
     */
    public static TetrominoEnum getRandomTetromino() {
      Random random = new Random();
      return values()[random.nextInt(values().length-1)];
    }
  }
}