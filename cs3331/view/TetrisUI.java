package cs3331.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;

import java.util.Observable;
import java.util.Observer;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import cs3331.model.Tetromino.TetrominoEnum;
import cs3331.view.animation.AnimationApplet;
import cs3331.model.GameState;
import cs3331.model.Tetromino;
import cs3331.controller.Controller;
import cs3331.model.Coordinates;

/**
 * Creates main user interface. See AnimationApplet to see what is being inherited.
 * @author epadilla2
 *
 */
@SuppressWarnings("serial")
public class TetrisUI extends AnimationApplet implements Observer {
  
  private boolean GamePaused = true;
  private GameState gameState;
  private ActionListener alController = null;
  private KeyListener klController = null;
  public static final Color BACK_PANEL_COLOR = new Color(169, 169, 169);
  public static final Color BACK_BOARD_COLOR = new Color(0, 0, 0);
  private NetworkInterface netInterface = new NetworkInterface();
  private static int[] $SWITCH_TABLE$cs3331$model$Tetromino$TetrominoEnum;


  public TetrisUI(String[] args) {
    super(args);
  }
  
  @Override
  public void init() {
    this.delay = 500;
    super.init();
  }
  
  @Override
  public void start() {
    super.start();
    this.repaint();
    this.GamePaused = false;
  }
  
  @Override
  public void stop() {
    super.stop();
    this.GamePaused = true;
  }
  /**
    * Helper method that returns true if the game if {@link #GamePaused} is true
    * @return returns true if {@link #GamePaused} is true
    */
  public boolean GamePaused() {
        return this.GamePaused;
    }
  
  /**
   * Sets a controller to be an ActionListener
   * @param controller the {@link #alController} of the {@link TetrisUI}
   */
  public void setActionListener(ActionListener controller) {
        this.alController = controller;
    }
  /**
   * Sets a controller to be a KeyListener
   * @param controller the {@link #klController} of the {@link TetrisUI}
   */
  public void setKeyListener(KeyListener controller) {
        this.klController = controller;
    }
  
  /** Returns a KeyListener for the UI 
   * @return this.klController The KeyListener of the {@link TetrisUI}*/
  @Override
    public KeyListener getKeyListener() {
        return this.klController;
    }
  
  /**
   * sets the model of the {@link TetrisUI} to be the {@link #gameState}
   * @param gameState the {@link #gameState} to be associated with the {@link TetrisUI}
   */
  public void setModel(GameState gameState) {
        this.gameState = gameState;
    }
  /** A Mutator class to adjust the statusBar text with a new String
   * @param text the String the statusBar is to be updated with */
  public void setStatusBarText(String text) {
        this.statusBar.setText(text);
    }
  
  /**Method to reset the timer.*/
  public void resetTimer() {
    this.timer = new Timer(this.delay, e -> this.periodicTask());
  }
  /**
   * Returns the {@link #netInterface} of the UI
   * @return netInterface the NetworkInterface of the UI
   */
  public NetworkInterface getNetworkInterface() {
    return this.netInterface;
  }
  
  
  
  /**
   * Moves the player's Tetromino down if the game is active or over
   */
  @Override
  public void periodicTask()
  {
    if (this.gameState.isGameActive() || this.gameState.isGameOver()) {
      this.gameState.moveTetrominoDown();
    }
    super.periodicTask();
  }
  /**
   * Here goes what is going to drawn on screen
   */
  protected void paintFrame(Graphics g) 
  {
    if (g == null) {
      return;
    }
    this.syncPaintFrame(g); 
  }
  /**
   * Synchronizes {@link #colorBoard(Graphics, int, int, int)} and {@link #colorState(Graphics)}
   * @param g java graphics
   */
  private synchronized void syncPaintFrame(Graphics g) {
        g.setColor(BACK_PANEL_COLOR);
        g.fillRect(0, 0, this.dim.width, this.dim.height);
        g.setColor(BACK_BOARD_COLOR);
        g.fillRect(25, 25, 300, 600);
        if (this.gameState.isGameActive() || this.gameState.isGameOver()) {
            this.colorTetrominos(g, this.gameState.getBoard(), 30, 25, 25);
        }
        this.colorBoard(g, 30, 25, 25);
        this.colorState(g);
    }
  /**
   * Colors the Tetrominoes on the board with their associated color
   * @param g a Java Graphics driver
   * @param board the Tetris board the Tetrominoes will be colored into
   * @param squareSize the size of a single square of the board
   * @param marginLeft the leftmost margin of the board
   * @param marginTop the topmost margin of the board
   */
  public void colorTetrominos(Graphics g, Tetromino.TetrominoEnum[][] board, int squareSize, int marginLeft, int marginTop) {
        for (int x = 0; x < 10; ++x) {
            for (int y = 0; y < 20; ++y) {
                if (board[y][x] == null) continue;
                g.setColor(this.getTetrominoColor(board[y][x]));
                g.fillRect(marginLeft + x * squareSize, marginTop + y * squareSize, squareSize, squareSize);
            }
        }
    }
  /**
   * Draws the board of the {@link TetrisUI}
   * @param g a Java Graphics driver
   * @param squareSize The size of the board to be drawn
   * @param marginLeft the leftmost margin of the board to be drawn
   * @param marginTop the topmost margin of the board to be drawn
   */
  public void colorBoard(Graphics g, int squareSize, int marginLeft, int marginTop) {
        int i;
        g.setColor(Color.LIGHT_GRAY);
        for (i = 0; i <= 10; ++i) {
            g.drawLine(marginLeft + i * squareSize, marginTop, marginLeft + i * squareSize, marginTop + squareSize * 20);
        }
        for (i = 0; i <= 20; ++i) {
            g.drawLine(marginLeft, marginTop + i * squareSize, marginLeft + squareSize * 10, marginTop + i * squareSize);
        }
    }
  /**
   * Decorates the UI with all the pertinent information of the Game State
   */
  protected void colorState(Graphics g) {
        int i;
        g.setColor(BACK_BOARD_COLOR);
        for (i = 0; i < 3; ++i) {
            g.drawRect(350 + i, 25 + i, 180, 125);
        }
        if (this.gameState.isGameActive()) {
            Tetromino nextTetronimo = this.gameState.getNextTetro();
            Iterator<Coordinates<Integer, Integer>> iter = nextTetronimo.iterator();
            Color tetroColor = this.getTetrominoColor(nextTetronimo.getType());
            while (iter.hasNext()) {
                Coordinates<Integer, Integer> pair = iter.next();
                g.setColor(tetroColor);
                g.fillRect(400 + pair.getX() * 25, 75 + pair.getY() * 25, 25, 25);
                g.setColor(Color.LIGHT_GRAY);
                g.drawRect(400 + pair.getX() * 25, 75 + pair.getY() * 25, 25, 25);
            }
        }
        g.setColor(Color.WHITE);
        g.setFont(new Font(this.getFont().getName(), 1, 24));
        g.drawString("Next Figure:", 360, 50);
        g.setColor(BACK_BOARD_COLOR);
        for (i = 0; i < 3; ++i) {
            g.drawRect(350 + i, 175 + i, 185, 135);
        }
        g.setColor(Color.WHITE);
        g.drawString("Level: " + this.gameState.getLevel(), 25, 20);
        g.drawString("Lines: " + this.gameState.getClrLines(), 150, 20);
        g.drawString("Score: " + this.gameState.getScore(), 275, 20);
        Tetromino.TetrominoEnum[][] opponentsBoard = this.gameState.getOtherBoard();
        if (this.gameState.isMultiPlayerGame()) {
            g.setColor(BACK_BOARD_COLOR);
            for (int i2 = 0; i2 < 3; ++i2) {
                g.drawRect(350 + i2, 335 + i2, 185, 290);
            }
            g.setColor(Color.WHITE);
            g.setFont(new Font(this.getFont().getName(), 1, 14));
            g.drawString(this.gameState.getOtherPlayerName(), 360, 355);
            g.drawString("Score: " + this.gameState.getOtherScore(), 360, 375);
            g.setColor(BACK_BOARD_COLOR);
            g.fillRect(390, 415, 100, 200);
            if (opponentsBoard != null) {
                this.colorTetrominos(g, opponentsBoard, 10, 390, 415);
            }
            this.colorBoard(g, 10, 390, 415);
        }
    }

  
  /**
   * Return a color based on the Tetromino type
   * @param tetrominoEnum the associated enum of a {@link cs3331.model.Tetromino}
   * @return color the color associated with the enum passed from the passed {@link cs3331.model.Tetromino}
   */
  private Color getTetrominoColor(TetrominoEnum tetrominoEnum)
  {
    Color color = null;
    switch (tetrominoEnum)
    {
      case I:
        color = Color.RED; break;
      case J:
        color = Color.GREEN; break;
      case L:
        color = Color.PINK; break;
      case O:
        color  = Color.CYAN; break;
      case S:
        color = Color.MAGENTA; break;
      case Z:
        color = Color.YELLOW; break;
      case T:
        color = Color.ORANGE; break;
      default:
        color =  Color.WHITE; break;
    }//end switch
    return color;
  }//end getTetrominoColor
  
  @Override
    protected JPanel createUI() {
        this.statusBar.setOpaque(true);
        this.statusBar.setBackground(BACK_BOARD_COLOR);
        this.statusBar.setText("");
        this.statusBar.setFont(new Font(this.statusBar.getName(), 1, 20));
        this.statusBar.setForeground(Color.WHITE);
        JPanel root = new JPanel();
        root.setLayout(new BorderLayout());
        root.add((Component)this.createMenuBar(), "North");
        root.add((Component)this, "Center");
        root.add((Component)this.statusBar, "South");
        return root;
    }
  /**
   * Creates the menu bar of the Tetris application
   * @return menuBar the finished menu
   */
  protected JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        menuBar.setBackground(Color.BLACK);
        JMenu optionsMenu = new JMenu("File");
        optionsMenu.setBackground(Color.BLACK);
        optionsMenu.setForeground(Color.WHITE);
        JMenuItem newGameMenu = new JMenuItem("New Game");
        newGameMenu.setBackground(Color.BLACK);
        newGameMenu.setForeground(Color.WHITE);
        newGameMenu.setActionCommand("New Game");
        newGameMenu.addActionListener(this.alController);
        JMenuItem instructionsMenu = new JMenuItem("Controls");
        instructionsMenu.setBackground(Color.BLACK);
        instructionsMenu.setForeground(Color.WHITE);
        instructionsMenu.addActionListener(new ActionListener(){

         /** Creates a dialog box if the player selects Controls from the menu
          * @param e the selection of Controls from the game menu */
            @Override
            public void actionPerformed(ActionEvent e) {
                TetrisUI.this.stop();
                JDialog instructionsDialog = new JDialog();
                instructionsDialog.setSize(350, 175);
                instructionsDialog.setTitle("Controls");
                StringBuilder sb = new StringBuilder();
                sb.append("Right Arrow Key: Move tetromino to the right.\n");
                sb.append("Left Arrow Key: Move tetromino to the left.\n");
                sb.append("Down Arrow Key: Push tetromino down.\n");
                sb.append("\"C\" Key: Rotate tetromino right.\n");
                sb.append("\"Z\" Key: Rotate tetromino left.\n");
                sb.append("Escape Key: Pause-Start game.\n");
                JTextArea instructionsText = new JTextArea(sb.toString());
                instructionsText.setEditable(false);
                instructionsText.setBackground(Color.WHITE);
                instructionsText.setForeground(Color.BLACK);
                instructionsText.setFont(new Font(TetrisUI.this.statusBar.getName(), 0, 16));
                instructionsDialog.add(new JScrollPane(instructionsText));
                instructionsDialog.setVisible(true);
            }
        });
        JMenuItem quitMenu = new JMenuItem("Quit");
        quitMenu.setBackground(Color.BLACK);
        quitMenu.setForeground(Color.WHITE);
        quitMenu.setActionCommand("Quit");
        quitMenu.addActionListener(this.alController);
        optionsMenu.add(newGameMenu);
        optionsMenu.add(instructionsMenu);
        optionsMenu.addSeparator();
        optionsMenu.add(quitMenu);
        menuBar.add(optionsMenu);
        return menuBar;
    }
  
  /**
   * When there is a change on the {@link #gameState}, the {@link TetrisUI} gets notified (this method is called)
   */
  @Override
  public void update(Observable obs, Object obj)
  {
    if (this.gameState.isGameOver()) {
            this.stop();
            this.delay = 500;
            this.resetTimer();
            this.statusBar.setText("Game Over!");
            if (this.gameState.isOtherGameOver()) {
                if (this.gameState.getScore() < this.gameState.getScore()) {
                    this.statusBar.setText("You Win!!!");
                } else if (this.gameState.getOtherScore() > this.gameState.getScore()) {
                    this.statusBar.setText("You Lose!!!");
                } else {
                    this.statusBar.setText("It's A Tie!!");
                }
            }
            return;
        }
        if (this.gameState.isOtherGameOver()) {
            if (this.gameState.getOtherScore() < this.gameState.getScore()) {
                this.statusBar.setText("You Are Winnig!!!");
            } else if (this.gameState.getOtherScore() > this.gameState.getScore()) {
                this.statusBar.setText("You Are Losing!!!");
            }
        }
        if (this.gameState.levelUpgrade()) {
            this.stop();
            this.delay = (int)((double)this.delay * 0.8);
            this.resetTimer();
            this.start();
        }
        this.repaint();
  }
  
  
  /**
   * A switch table for sending Tetrominos and Filler Lines to the Other Player
   * @return $SWITCH_TABLE$cs3331$model$Tetromino$TetrominoEnum the Switch Table
   */
  static int[] $SWITCH_TABLE$cs3331$model$Tetromino$TetrominoEnum() {
       int[] arrn = new int[Tetromino.TetrominoEnum.values().length];
        if ($SWITCH_TABLE$cs3331$model$Tetromino$TetrominoEnum != null) {
            return arrn;
        }
        try {
            arrn[Tetromino.TetrominoEnum.FILLER.ordinal()] = 8;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            arrn[Tetromino.TetrominoEnum.I.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            arrn[Tetromino.TetrominoEnum.J.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            arrn[Tetromino.TetrominoEnum.L.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            arrn[Tetromino.TetrominoEnum.O.ordinal()] = 4;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            arrn[Tetromino.TetrominoEnum.S.ordinal()] = 5;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            arrn[Tetromino.TetrominoEnum.T.ordinal()] = 7;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            arrn[Tetromino.TetrominoEnum.Z.ordinal()] = 6;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        $SWITCH_TABLE$cs3331$model$Tetromino$TetrominoEnum = arrn;
        return $SWITCH_TABLE$cs3331$model$Tetromino$TetrominoEnum;
    }
}//end TetrisUI class
